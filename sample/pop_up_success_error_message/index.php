<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Show popup success/error message for few seconds using jQuery</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
<!-- http://www.itechroom.com-->
function checkUserName(usercheck)
{
	var uname = document.getElementById('username').value;
	$.post("checkuser.php", {user_name: uname} , function(data)
		{			
			   if (data != '' || data != undefined || data != null) 
			   {				   
				  $('#usercheck').show();
				  $('#usercheck').html(data);
				  setTimeout("$('#usercheck').hide(); ", 3000); //display message for 3 seconds
			   }
          });
}

function checkUserLogin(user_id)
{
   if (user_id == '' || user_id == undefined || user_id == null) 
   {				   
	  $('#logincheck').show();
	  $('#logincheck').css('color', '#CC0000');
	  $('#logincheck').html('You must be logged in to submit.');
	  setTimeout("$('#logincheck').hide(); ", 3000); //display message for 3 seconds
   }
   else {
   	  document.frmPopMessage.submit();
   }

}
</script>
</head>
<body>
<div  style="width:800px; margin:0 auto;">
<div style=" padding:20px 0px;"><h2>Show popup success/error message for few seconds using jQuery</h2></div>

<form name="frmPopMessage" id="frmPopMessage" method="post" action="">
<div> Example 1: Checking username availability</div>
<div id="usercheck" class="popup_error"></div>
<div style="padding:10px 0px  10px 10px;">
Username : <input type="text" name="username" id="username"  /> 
</div>
<div style="padding:10px 0px  50px 10px;">
<input type="button" name="btnSubmit" value="Submit" onclick="checkUserName();"/>
</div>
<br />
<div> Example 2: Checking user Login</div>
<div id="logincheck" class="popup_error"></div>
<div style="padding:10px 0px  50px 10px;">
<input type="button" name="btnSubmit" value="Click to Submit" onclick="checkUserLogin(<?php echo $_SESSION['user_id'];?>);"/>
</div>
</form>

<div style="padding:0px 0px  50px 10px;"> For testing, registered username are: itechroom, trialuser </div>
<div align="center"><h3>Visit <a href="http://wwww.itechroom.com" target="_blank"> www.itechroom.com</a> for more.</h3></div>
</div>
</body>
</html>
